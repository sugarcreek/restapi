const mysql = require('mysql2/promise');
const config = require('../config');

async function query(sql, params) {
  const connection = await mysql.createConnection(config.db);
  // console.log(sql);
  // console.log(params);
  // console.log(connection);
  // !! issue with MySQL 8 prepare statement changes connection.execute doesn't work anymore !!
  const [results, ] = await connection.query(
    sql, 
    params,
    function(err, results, fields) {
      console.log(results); // results contains rows returned by server
      console.log(fields); // fields contains extra meta data about results, if available
    }
    );

  return results;
}

module.exports = {
  query
}