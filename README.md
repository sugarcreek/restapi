# README #

A basic REST API example. One written in PHP, the other one written in Javascript using Node, Express.
Both API retrieve data from a MySQL database.

### What is this repository for? ###

* Resume showcase

### How do I get set up? ###

* This repo is solely used as a storage location with benefits for now.

### Contribution guidelines ###

* None needed or wanted

### Who do I talk to? ###

* Repo owner or admin
